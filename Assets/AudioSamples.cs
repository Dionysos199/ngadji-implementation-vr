using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioSamples : MonoBehaviour
{
    AudioSource audioSource;
    public GameObject soundEmitter;
    public float[] audioData = new float[512];
    // Start is called before the first frame update
    Rigidbody rb;
    GameObject[] cubes;
    public GameObject cube;


    public bool trigger = false;
    void Start()
    {
        
        audioSource = soundEmitter.GetComponent<AudioSource>();

        cubes = new GameObject[512];
        createWall(15);


    }
    void createWall(float side)
    {
        int n = 0;
        for (int i = 0; i < side; i++)
        {
            for (int j = 0; j < side; j++)
            {
                n++;
                GameObject block = Instantiate(cube, transform);
                cubes[n] = block;
                block.transform.position = transform.position+ new Vector3(0, j * cube.transform.localScale.y, i * cube.transform.localScale.x);

            }

        }
    }

    void scaleFn()
    {
        int n = 0;
        foreach (var item in cubes)
        {
            n++; 
            if (item)
            {
                item.transform.localScale = new Vector3(1+10 * audioData[n],1, 1);

            }
        }

    }
    void explosion()
    {
        int n = 0;
        foreach (var item in cubes)
        {
            n++;
            if (item)
            {
                if (!item.GetComponent<Rigidbody>())
                {
                    item.AddComponent<Rigidbody>();
                    rb = item.GetComponent<Rigidbody>();
                }
                rb.AddForce(new Vector3(100000 * audioData[n],0, 0));
            }
        }

    }
    // Update is called once per frame


    void Update()
    {
        audioSource.GetSpectrumData(audioData, 0, FFTWindow.Blackman);
        scaleFn();
        if (trigger)
        {
            explosion();
        }
    }
}
