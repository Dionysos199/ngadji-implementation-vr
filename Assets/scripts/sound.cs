using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sound : MonoBehaviour
{
    GameObject drumStick;
    AudioSource drumhitSound;
    public GameObject effect;

    public GameObject wall;
    public GameObject explosionPos;
    private bool explode;
    // Start is called before the first frame update
    private void FixedUpdate()
    {
        if (explode)
        {
            Rigidbody rb = wall.GetComponent<Rigidbody>();
          //  rb.AddForce(new Vector3(10, 10, 10));

           // rb.AddExplosionForce(100,explosionPos.transform.position,20);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        drumhitSound = GetComponent<AudioSource>();
     
        if (other.gameObject.name == "drumStick")
        {

            explode = true;
            drumhitSound.Play();
            Vector3 position = other.transform.position;

            GameObject _effect = Instantiate(effect, position, Quaternion.identity);
            Destroy(_effect, 3);
        }
        Debug.Log(other.gameObject.name);

    }
   /* private void OnCollisionEnter(Collision collision)
    {
        drumhitSound = GetComponent<AudioSource>();

        if (collision.gameObject.name == "drumStick")
        {

            drumhitSound.Play();

        }
        Debug.Log(collision.gameObject.name);
    }*/

}
